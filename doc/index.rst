.. GEM Online Analysis documentation master file, created by
   sphinx-quickstart on Tue May 26 11:03:35 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GEM Online Analysis's documentation!
===============================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   notebooks/test_unpacker.ipynb
   CONTRIBUTING
   DEVELOPERS
   gdh/gdh



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
