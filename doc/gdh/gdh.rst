API documentation
**********************
Executable scripts
==================

.. automodule:: gdh.unpacker
    :members:

Utils
===================

.. autoclass:: gdh.utils.IndexedNDArray
    :show-inheritance:
    :members:

Data Format Details
===================

.. autoclass:: gdh.formats.generic_block.GenericBlock
    :members:

.. autoclass:: gdh.formats.cdf.CDF
    :show-inheritance:
    :members:

.. autoclass:: gdh.formats.amc13.AMC13
    :show-inheritance:
    :members:

.. autoclass:: gdh.formats.amc.AMC
    :show-inheritance:
    :members:

.. autoclass:: gdh.formats.geb.GEB
    :show-inheritance:
    :members:

.. autoclass:: gdh.formats.vfat.VFAT
    :show-inheritance:
    :members:
