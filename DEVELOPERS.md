# Developing GEM Online Analysis

* [Development Setup](#setup)
* [Running Tests](#tests)
* [Code Style Guidelines](#rules)
* [Writing Documentation](#documentation)

## <a name="setup"> Development Setup
### Development requirements

Recommended way of development is under virtual environment. Same instructions are applicable
to non-virtual environment too, but it is not advised. Use at your own risk.
First of all, you need to have `python` of version `>=3.6` available on your development PC.
Ensuring this is your own responsibility, as different operation systems are supplied with
different python interpreters and different methods are used to install modern python interpreter
alongside with the system one. At CERN's CC7 lxplus machines the recommended way is to [source
an appropriate software collection](https://cern.service-now.com/service-portal?id=kb_article&n=KB0000730).
On official GEM development PCs `python3` is already installed.

```shell
python3 -m venv /path/to/venv/ # create your python3 virtual environment, to be done only once
source /path/to/venv/bin/activate # activate your virtual environment, to be done every time you want to develop/test
python3 -m pip install --upgrade pip # optional, if you want to upgrade pip to the most recent version, to be done only once
python3 -m pip install flit # required for project building and installation, to be done only once
deactivate # return from virtual environment to the regular one
```

### Installation instructions

* Clone the repository `git clone https://gitlab.cern.ch/cmsgemonline/gem-online-analysis.git && cd gem-online-analysis`

* Activate your virtual environment `source /path/to/venv/bin/activate`
* Install the `gdh`(GEM Data Handler) package in your virtual environment `flit -f pyproject_gdh.toml install --symlink` 

## <a name="tests"> Running Tests

### Checking whether your code is up to project standard
Use [*pylint*][pylint] to check if your code quality is good. Standard linter configuration is used.
Compare the linter ratings before and after your modifications and do not commit the code which reduces the code quality.

### Testing the code with Jupyter notebooks

Tests alongside with some supporting documentation are provided in `jupyter` [notebook][./doc/notebooks/test_unpacker.ipynb].
`Jupyter` will be installed as a dependency with the `gdh` package. You can add your own test notebooks when required.
In order to run the notebook locally simply run 

`jupyter notebook /path/to/gem-online-analysis/doc/notebooks/test_unpacker.ipynb`

In order to run the notebook on remote PC, please see below an instruction for one of the official GEM development PCs:

1. Start jupyter notebook on remote PC. Override default port with 8889 or other port number of your choice.

```shell
jupyter notebook --no-browser --port=8889 --ip=0.0.0.0 /path/to/test_unpacker.ipynb
```
This command will show you a link to `localhost` which you will have to use. Copy it to your clipboard.

2. On your local PC setup an ssh port forwarding:

```shell
ssh -vL 8889:gem904daq01.cern.ch:8889 -oProxyCommand="ssh <your_lxplus_name>@lxplus7.cern.ch -W %h:%p" <your_lxplus_name>@gem904daq01.cern.ch
```

3. Open the link you've copied in step 1 in your browser. Enjoy!


## <a name="rules"> Code Style Guidelines
We are following WebKit [Code Style Guideline][webkit] for `C++` and [PEP8][pep8] standard for python code.
We also find very useful [C++ Core Guidelines][cppcore] by Bjarne Stroustrup and Herb Sutter and strongly encourage you to read them too.

## <a name="documentation"> Writing Documentation
We use the [sphinx][sphinx] with read-the-docs theme for `python` code documentation.
Additional dependencies to install:
- `sphinx`
- `sphinx_rtd_theme`
- `recommonmark`
- `nbsphinx`

*Note:* `nbsphinx` *uses* [*pandoc*][pandoc] *to convert markdown cells. If you don't have it installed in your system, you should install it manually.*

The documentation can be generated using the following command:

```bash
sphinx-build -b html doc  build/doc
```

Generated files are placed in `build/doc`.

This section will be completed later with precise documentation style guide, for the moment follow the examples from the code itself.

[webkit]:https://webkit.org/code-style-guidelines/
[cppcore]:https://github.com/isocpp/CppCoreGuidelines/
[pep8]:https://www.python.org/dev/peps/pep-0008/
[doxygen]:http://www.doxygen.nl/manual/starting.html
[sphinx]:https://www.sphinx-doc.org/en/master/usage/quickstart.html
[pandoc]:https://pandoc.org/installing.html
[pylint]:https://www.pylint.org/
