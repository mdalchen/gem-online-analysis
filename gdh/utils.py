# pylint: disable=missing-module-docstring
# This module contains only one class which is properly documented

import numpy as np

class IndexedNDArray(np.ndarray):
    """
    Helper class providing ndarray with extra ``pos`` attribute.
    This attribute will be used to indicate current position
    when moving through array values.
    See `numpy example. <https://numpy.org/
    doc/stable/user/basics.subclassing.html>`_
    """

    # pylint: disable=multiple-statements
    # pylint: disable=attribute-defined-outside-init

    def __new__(cls, input_array, pos=0):
        obj = np.asarray(input_array).view(cls)
        obj.pos = pos
        return obj

    def __array_finalize__(self, obj):
        if obj is None: return
        self.pos = getattr(obj, 'pos', 0)
