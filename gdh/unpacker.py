"""
Executable script for unpacking GEM binary data
"""

import argparse
import logging
import pandas as pd
import numpy as np
from gdh.utils import IndexedNDArray
from gdh.formats.cdf import CDF

logger = logging.getLogger('gdh.unpacker')
np.set_printoptions(formatter={'int':hex})

def run(filename, source, nrows=None, verbosity=1):
    """
    Unpack gem binary file

    :param str filename: Input data binary file
    :param str source: Type of binary stream source
    :param int nrows: Number of events to unpack
    :param int verbosity: Logging level

    :return pandas.DataFrame: A table of events with column names as tuples.
        Each row corresponds to one event
    """

    if verbosity == 0:
        # suppress all logging except errors
        logging.basicConfig(level=logging.ERROR)
    elif verbosity == 1:
        # set log level to INFO
        logging.basicConfig(level=logging.INFO)
    else:
        # set level to DEBUG
        logging.basicConfig(level=logging.DEBUG)

    logger.info('Processing file: %s. \n Data source: %s', filename, source)
    cbs = IndexedNDArray(np.fromfile(filename, dtype='>Q').byteswap(inplace=True), pos=0)
    logger.debug('Input data size in 64-bit words: %d', cbs.size)
    # TODO read first word and determine the source type instead of asking to pass
    if source in ('minidaq', 'ferol'):
        logger.warning('%s format is not supported at the moment', source)
        return None

    # Create the CDF instance (which creates every format block downstream).
    # This has to be done outside of the loop as the formats are precompiled
    # during creation of the objects and there's no need to create them on
    # per-event basis
    cdf = CDF()
    # Get one event from the stream
    def get():
        while cbs.pos < cbs.size:
            yield cdf.unpack(cbs)
            logger.debug('Data size in 64-bit words: %d. Position: %d', cbs.size, cbs.pos)

    events = pd.DataFrame.from_records(get(), nrows=nrows)
    logger.info(events.head())
    logger.info('Finished unpacking. Processed %d events.', len(events.index))
    return events

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Unpack GEM row binary file')
    parser.add_argument('ifilename', type=str,
                        help='Input file name')
    parser.add_argument('isource', type=str, choices=['sdram', 'ferol', 'minidaq'],
                        help='Source type. Only "sdram" is currently supported')
    parser.add_argument('-n', '--nevents', dest='nevents', type=int, default=None,
                        help='Maximum number of events to process')
    parser.add_argument('-v', '--verbosity', dest='verbosity', type=int,
                        choices=[0, 1, 2], default=1,
                        help='Logger verbosity levels')

    args = parser.parse_args()

    run(args.ifilename, args.isource, args.nevents, args.verbosity)
