# pylint: disable=missing-module-docstring
# This module contains only one class which is properly documented

from abc import ABC
import logging

class GenericBlock(ABC):
    """
    Abstract class defining genericl binary stream block
    See detailed data format `description. <https://docs.google.com/
    spreadsheets/d/1iKMl68vMsSWgr8ekVdsJYTk7-Pgy8paxq98xj6GtoVo/edit?usp=sharing>`_

       .. note ::
          This is a base class and it must be extended
    """
    def __init__(self):
        """
        Default constructor
        """
        self.logger = logging.getLogger('gdh.formats.' + self.__class__.__name__)
        logging.basicConfig(level=logging.INFO)
        self.slot = None
        self.link = None
        self.pos = None
        self.block_id = 'Generic Block'
        self.setup()
        self.compile()

    def update_key(self, key, sub_block='PAYLOAD'):
        """
        Convert filed name to a tuple with hardware tagging information

        :param str key: Field name
        :param str sub_block: Sub block id

        :return tuple: ``('FIELD NAME', 'BLOCK ID', 'SUB BLOCK',
            self.slot, self.link, self.pos)``
        """
        return (key, self.block_id, sub_block, self.slot, self.link, self.pos)

    def unpack_word(self, stream, compiled_dict, n_words=1):
        """
        Read and unpack a word from stream

        :param IndexedNDArray stream: An array of 64-bits data words
        :param dict compiled_dict: Compiled ``bitstruct`` dictionary
            with word fields in form ``{'Field Name':'Field Type:length'}``
        :param int n_words: Number of 64-bit words in the array

        :return dict: Dictionary in form ``{'Field Name': Value}``
        """
        self.logger.debug("unpack_word called. Stream position %d, word: %s",
                          stream.pos, stream[stream.pos])
        unpacked_word = compiled_dict.unpack(
            stream[stream.pos:stream.pos + n_words].tobytes())
        stream.pos += n_words
        self.logger.debug("word reading done. Stream position %d", stream.pos)
        self.logger.debug('unpacked word %s', unpacked_word)
        return unpacked_word

    def compile(self):
        """
        Compile bitstruct dictionary.
        Virtual method, implemented in descendant classes
        """

    def setup(self):
        """
        Define block data fields.
        Virtual method, implemented in descendant classes
        """

    def unpack(self, stream):
        """
        Unpack block.
        Virtual method, implemented in descendant classes.

        :param IndexedNDArray stream: An array of 64-bits data words

        :return dict: Dictionary in form ``{Column name tuple: Value}``
        """

    def pack(self):
        """
        Placeholder. TBU
        """

    def print(self):
        """
        Placeholder. TBU
        """

    def set_logger_console_handler(self):
        """
        Placeholder. TBU
        """

    def set_logger_file_handler(self):
        """
        Placeholder. TBU
        """
