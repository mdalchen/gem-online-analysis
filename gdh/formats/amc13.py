# pylint: disable=missing-module-docstring
# This module contains only one class which is properly documented

import cbitstruct as bs
from gdh.formats.generic_block import GenericBlock
from gdh.formats.amc import AMC

class AMC13(GenericBlock):
    """
    CMS AMC13 data format block.
    See detailed data format `description. <https://docs.google.com/
    spreadsheets/d/1iKMl68vMsSWgr8ekVdsJYTk7-Pgy8paxq98xj6GtoVo/edit?usp=sharing>`_

        .. todo ::
            provide detailed description here
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self):
        """
        Default constructor.
        Creates child ``AMC()`` and set self block id to 'AMC13'
        """
        super().__init__()
        self.amc = AMC()
        self.block_id = 'AMC13'

    def setup(self):
        """
        Define self header and trailer fields
        (two 64-bit word header and one 64-bit word trailer)
        """
        self.header_fields = {'uFOV'    :'u4',
                              'CAL TYPE':'u4',
                              'N AMC'   :'u4',
                              'RES(0)'  :'u16',
                              'ORBIT N' :'u32',
                              'PAD'     :'u4'}


        self.amc_header_fields = {'AMC:$:B'        :'u1', #empty bit
                                  'AMC:$:L'        :'u1',
                                  'AMC:$:M'        :'u1',
                                  'AMC:$:S'        :'u1',
                                  'AMC:$:E'        :'u1',
                                  'AMC:$:P'        :'u1',
                                  'AMC:$:V'        :'u1',
                                  'AMC:$:C'        :'u1',
                                  'AMC:$:AMC SIZE' :'u24',
                                  'AMC:$:PAD'      :'u4',
                                  'AMC:$:BLK SEQ N':'u8',
                                  'AMC:$:AMC N'    :'u4',
                                  'AMC:$:BOARD ID' :'u16'}

        self.trailer_fields = {'CRC32' :'u32',
                               'PAD'   :'u4',
                               'BLK N' :'u8',
                               'L1A ID':'u8',
                               'BX ID' :'u12'}

    def compile(self):
        """
        Compile self header and trailer fields
        (two 64-bit word header and one 64-bit word trailer)
        """
        self.header_compiled = bs.compile(
            ''.join(list(self.header_fields.values())),
            list(self.header_fields.keys()))
        self.amc_header_compiled = bs.compile(
            ''.join(list(self.amc_header_fields.values())),
            list(self.amc_header_fields.keys()))
        self.trailer_compiled = bs.compile(
            ''.join(list(self.trailer_fields.values())),
            list(self.trailer_fields.keys()))

    def unpack(self, stream):
        """
        Unpack AMC13 block. Unpacking sequence:

        * AMC13 header
        * AMC13::AMC header fields (read their number from AMC13 header)
        * AMC blocks
        * AMC13trailer

        :param IndexedNDArray stream: An array of 64-bits data words

        :return dict: Dictionary in form ``{Column name tuple: Value}``
        """
        event = {self.update_key(k, 'HEADER'): v
                 for k, v in
                 self.unpack_word(stream, self.header_compiled).items()}
        # Loop over AMC13:H:N AMC headers
        for _ in range(event[('N AMC',
                              self.block_id,
                              'HEADER',
                              self.slot,
                              self.link,
                              self.pos)]):
            # NOTE AMC number ranges from 0 to (N AMC-1). Shall add "+1"?
            temp_amc_dict = self.unpack_word(stream, self.amc_header_compiled)
            temp_amc_dict_2 = {f'{k[:4]}{temp_amc_dict["AMC:$:AMC N"]}{k[5:]}': v
                               for k, v in temp_amc_dict.items()}
            event.update({self.update_key(k): v
                          for k, v in temp_amc_dict_2.items()})

        for _ in range(event[('N AMC',
                              self.block_id,
                              'HEADER',
                              self.slot,
                              self.link,
                              self.pos)]):
            self.amc.slot = temp_amc_dict['AMC:$:AMC N']
            event.update(self.amc.unpack(stream))
        self.logger.debug("all AMCs payload reading done. Stream position %d",
                          stream.pos)
        event.update({self.update_key(k, 'TRAILER'): v
                      for k, v in
                      self.unpack_word(stream, self.trailer_compiled).items()})
        self.logger.debug("AMC13 payload reading done. Stream position %d",
                          stream.pos)
        return event
