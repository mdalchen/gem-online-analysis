# pylint: disable=missing-module-docstring
# This module contains only one class which is properly documented

import cbitstruct as bs
from gdh.formats.generic_block import GenericBlock

class VFAT(GenericBlock):
    """
    GEM AMC block.
    See detailed data format `description. <https://docs.google.com/
    spreadsheets/d/1iKMl68vMsSWgr8ekVdsJYTk7-Pgy8paxq98xj6GtoVo/edit?usp=sharing>`_

        .. todo ::
            provide detailed description here
    """
    def __init__(self):
        """
        Default constructor. Set self block id to 'VFAT'
        """
        super().__init__()
        self.block_id = 'VFAT'
        self.payload_type = 0

    def setup(self):
        """
        Define self header and trailer fields.

            .. note ::
                Lossless VFAT data format:
                192 bits = 3 64 bit words

            .. warning ::
                ``cbitstruct`` package has a limitation
                on max field size of 64 bits.
                Thus, VFAT channel data is presented as
                a collection of two 64-bit fields:

                * 'CHANNEL DATA M' - Most significant 64 bits
                * 'CHANNEL DATA L' - List significant 64 bits

            .. todo ::
                Implement zero suppression and calibration data formats
        """
        self.vfat_block = {'POS'           :'u8',
                           'PAD'           :'u7',
                           'VC'            :'u1',
                           'HEADER'        :'u8',
                           'EC'            :'u8',
                           'BC'            :'u16',# FIXME change to BX for uniformity?
                           'CHANNEL DATA M':'u64',
                           'CHANNEL DATA L':'u64',
                           'CRC'           :'u16'}

    def compile(self):
        """
        Compile complete VFAT block
        """
        self.vfat_block_compiled = bs.compile(
            ''.join(list(self.vfat_block.values())),
            list(self.vfat_block.keys()))

    def unpack(self, stream):
        """
        Unpack VFAT block. Unpacking sequence:

        * Unpack block to a temporary dictionary
        * Read VFAT position from temporary dictionary
        * Set self.pos and update dictionary key tuple

        :param IndexedNDArray stream: An array of 64-bits data words

        :return dict: Dictionary in form ``{Column name tuple: Value}``
        """
        vfat_block_unpacked = self.unpack_word(stream, self.vfat_block_compiled, 3)
        self.pos = vfat_block_unpacked['POS']
        return {self.update_key(k): v for k, v in vfat_block_unpacked.items()}
