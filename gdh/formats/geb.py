# pylint: disable=missing-module-docstring
# This module contains only one class which is properly documented

import cbitstruct as bs
from gdh.formats.generic_block import GenericBlock
from gdh.formats.vfat import VFAT

class GEB(GenericBlock):
    """
    GEM GEB block.
    See detailed data format `description. <https://docs.google.com/
    spreadsheets/d/1iKMl68vMsSWgr8ekVdsJYTk7-Pgy8paxq98xj6GtoVo/edit?usp=sharing>`_

        .. todo ::
            provide detailed description here
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self):
        """
        Default constructor. Creates child ``VFAT()`` and set self block id to 'GEB'
            .. todo ::
                Implement zero suppression payload types
        """
        super().__init__()
        self.slot = 0
        self.link = 0
        self.block_id = 'GEB'
        self.payload_type = 0 # TODO implement zero suppression payload types
        self.vfat = VFAT()

    def setup(self):
        """
        Define self header and trailer fields (one 64-bit word each)
        """
        self.header_fields = {'PAD1'        :'u17',
                              'CAL CHAN'    :'u7',
                              'INPUT ID'    :'u5',
                              'N VFAT WORDS':'u12',
                              'EVT F'       :'u1',
                              'IN F'        :'u1',
                              'L1A F'       :'u1',
                              'EVT SZ OFW'  :'u1',
                              'EVT NF'      :'u1',
                              'IN NF'       :'u1',
                              'L1A NF'      :'u1',
                              'EVT SZ WARN' :'u1',
                              'NO VFAT MARK':'u1',
                              'OOS AV'      :'u1',
                              'OOS VV'      :'u1',
                              'BX MM AV'    :'u1',
                              'BX MM VV'    :'u1',
                              'PAD2'        :'u10'}

        self.trailer_fields = {'PAD1'        :'u16',
                               'N VFAT WORDS':'u12',
                               'EVT UFW'     :'u1',
                               'STUCK DATA'  :'u1',
                               'IN UFW'      :'u1',
                               'PAD2'        :'u33'}

    def compile(self):
        """
        Compile self header and trailer fields (one 64-bit word each)
        """
        self.header_compiled = bs.compile(
            ''.join(list(self.header_fields.values())),
            list(self.header_fields.keys()))
        self.trailer_compiled = bs.compile(
            ''.join(list(self.trailer_fields.values())),
            list(self.trailer_fields.keys()))

    def unpack(self, stream):
        """
        Unpack GEB block. Unpacking sequence:

        * GEB header
        * VFAT blocks
        * GEB trailer

            .. note ::
                Currently only lossless data is supported.
                In this case, the size of VFAT payload is always 192 bits each.
                Number of VFAT blocks is then determined as number of VFAT words // 3.

        :param IndexedNDArray stream: An array of 64-bits data words

        :return dict: Dictionary in form ``{Column name tuple: Value}``
        """
        tmp = self.unpack_word(stream, self.header_compiled)
        event = {self.update_key(k, 'HEADER'): v
                 for k, v in tmp.items()}
        for _ in range(tmp['N VFAT WORDS']//3):
            self.vfat.slot = self.slot
            self.vfat.link = self.link
            self.vfat.payload_type = self.payload_type
            event.update(self.vfat.unpack(stream))
        self.logger.debug("all VFATs payload reading done. Stream position %d",
                          stream.pos)
        event.update({self.update_key(k, 'TRAILER'): v
                      for k, v in
                      self.unpack_word(stream, self.trailer_compiled).items()})
        self.logger.debug("GEB:%d:%d payload reading done. Stream position %d",
                          self.slot, self.link, stream.pos)
        return event
