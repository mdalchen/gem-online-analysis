# pylint: disable=missing-module-docstring
# This module contains only one class which is properly documented

import cbitstruct as bs
from gdh.formats.generic_block import GenericBlock
from gdh.formats.geb import GEB

class AMC(GenericBlock):
    """
    GEM AMC block.
    See detailed data format `description. <https://docs.google.com/
    spreadsheets/d/1iKMl68vMsSWgr8ekVdsJYTk7-Pgy8paxq98xj6GtoVo/edit?usp=sharing>`_

        .. todo ::
            provide detailed description here
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self):
        """
        Default constructor. Creates child ``GEB()``
        and set self block id to 'AMC'
        """
        super().__init__()
        self.slot = 0
        self.block_id = 'AMC'
        self.geb = GEB()

    def setup(self):
        """
        Define self header and trailer fields.

            .. note ::
                See data format description and method implementation for
                details since the format is data-driven.
                Variable format options are implemented as tuple.
        """
        # first 72 bits of 192-long header
        self.header_1_fields = {'PAD'      :'u4',
                                'AMC N'    :'u4',
                                'L1A ID'   :'u24',
                                'BX N'     :'u12',
                                'DATA LGTH':'u20'}

        self.header_2_start_fields = {'FV'      :'u4',
                                      'RUN TYPE':'u4'}

        # 24 bits optional run parameters
        self.run_parameters = ({'RP:PAD'          :'u24'},#Transition
                               {'RP:PAD'          :'u11', #Physics
                                'RP:PULSE STRETCH':'u3',
                                'RP:LATENCY'      :'u10'},
                               {'RP:PAD'          :'u1',  #Latency
                                'RP:EXT TRG'      :'u1',
                                'RP:IS C'         :'u1',
                                'RP:CAL DAC'      :'u8',
                                'RP:PULSE STRETCH':'u3',
                                'RP:LATENCY'      :'u10'},
                               {'RP:PAD'          :'u1',  #Threshold
                                'RP:SE IC'        :'u2',
                                'RP:THR ARM DAC'  :'u8',
                                'RP:PULSE STRETCH':'u3',
                                'RP:THR ZCC DAC'  :'u10'},
                               {'RP:PAD'          :'u2',  #S-curve
                                'RP:IS C'         :'u1',
                                'RP:CAL DAC'      :'u8',
                                'RP:PULSE STRETCH':'u3',
                                'RP:THR ARM DAC'  :'u10'})

        # last 96 bits of header
        self.header_2_end_fields = {'OR N'    :'u16',
                                    'BOARD ID':'u16'}

        self.header_3_fields = {'DAV LIST'    :'u24',
                                'BUF STATUS'  :'u24',
                                'DAV CNT'     :'u5',
                                'PAYLOAD VAR' :'u3',
                                'PAYLOAD TYPE':'u4',
                                'TTS'         :'u4'}

        # 128 bit trailer
        self.trailer_1_fields = {'LINK TO'  :'u24',
                                 'OOS'      :'u1',
                                 'PAD1'     :'u31',
                                 'BP'       :'u1',
                                 'ML'       :'u1',
                                 'CL'       :'u1',
                                 'DR'       :'u1',
                                 'BCL'      :'u1',
                                 'PAD2'     :'u3'}

        self.trailer_2_fields = {'CRC32'    :'u32',
                                 'L1A ID'   :'u8',
                                 'PAD3'     :'u4',
                                 'DATA LGTH':'u20'}

    def assemble_header_2(self, run_parameter):
        """
        Assemble second header of the AMC block based on the run type
        """
        tmp = {}
        tmp.clear()
        tmp.update(self.header_2_start_fields)
        tmp.update(run_parameter)
        tmp.update(self.header_2_end_fields)
        self.logger.debug("Assembled header 2 is: %s", tmp)
        return tmp

    def compile(self):
        """
        Compile self header and trailer fields
        (three 64-bit word header and two 64-bit word trailer)
        """
        self.header_1_compiled = bs.compile(
            ''.join(list(self.header_1_fields.values())),
            list(self.header_1_fields.keys()))
        self.header_2_start_compiled = bs.compile(
            ''.join(list(self.header_2_start_fields.values())),
            list(self.header_2_start_fields.keys()))
        # assemble 5 options of header 2 and compile
        header_2_array = [self.assemble_header_2(rp) for rp in self.run_parameters]
        self.logger.debug("Header 2 array: %s", header_2_array)
        self.header_2_array_compiled = tuple(bs.compile(
            ''.join(list(header2.values())),
            list(header2.keys())) for header2 in header_2_array)
        self.header_3_compiled = bs.compile(
            ''.join(list(self.header_3_fields.values())),
            list(self.header_3_fields.keys()))
        self.trailer_1_compiled = bs.compile(
            ''.join(list(self.trailer_1_fields.values())),
            list(self.trailer_1_fields.keys()))
        self.trailer_2_compiled = bs.compile(
            ''.join(list(self.trailer_2_fields.values())),
            list(self.trailer_2_fields.keys()))

    def unpack(self, stream):
        """
        Unpack AMC block. Unpacking sequence:

        * AMC headers (building second header on the fly)
        * GEB blocks. Use "DAV_LIST" to determine the active optical links
        * AMC trailers

        :param IndexedNDArray stream: An array of 64-bits data words

        :return dict: Dictionary in form ``{Column name tuple: Value}``
        """
        event = {self.update_key(k, 'HEADER'): v
                 for k, v in
                 self.unpack_word(stream, self.header_1_compiled).items()}
        temp_dict = self.unpack_word(stream, self.header_2_start_compiled)
        stream.pos = stream.pos - 1
        run_type = temp_dict['RUN TYPE']
        self.logger.debug("Run type: %d", run_type)
        event.update({self.update_key(k, 'HEADER'): v
                      for k, v in
                      self.unpack_word(stream, self.header_2_array_compiled[run_type]).items()})
        event.update({self.update_key(k, 'HEADER'): v
                      for k, v in
                      self.unpack_word(stream, self.header_3_compiled).items()})
        # Retrieve active optical links and retreieve payloads
        dav_list = event[('DAV LIST', self.block_id, 'HEADER', self.slot, self.link, self.pos)]
        for link in range(24):
            if (dav_list >> link) & 0x1:
                self.geb.slot = self.slot
                self.geb.link = link
                event.update(self.geb.unpack(stream))
        self.logger.debug("all GEBs payload reading done. Stream position %d", stream.pos)
        event.update({self.update_key(k, 'TRAILER'): v
                      for k, v in
                      self.unpack_word(stream, self.trailer_1_compiled).items()})
        event.update({self.update_key(k, 'TRAILER'): v
                      for k, v in
                      self.unpack_word(stream, self.trailer_2_compiled).items()})
        self.logger.debug("AMC:%d payload reading done. Stream position %d", self.slot, stream.pos)
        return event
