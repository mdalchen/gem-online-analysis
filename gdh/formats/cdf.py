# pylint: disable=missing-module-docstring
# This module contains only one class which is properly documented

import cbitstruct as bs
from gdh.formats.generic_block import GenericBlock
from gdh.formats.amc13 import AMC13


class CDF(GenericBlock):
    """
    CMS Common Data Format block.
    See detailed data format `description. <https://docs.google.com/
    spreadsheets/d/1iKMl68vMsSWgr8ekVdsJYTk7-Pgy8paxq98xj6GtoVo/edit?usp=sharing>`_

        .. todo ::
            provide detailed description here
    """
    def __init__(self):
        """
        Default constructor. Creates child ``AMC13()`` and set self block id to 'CDF'
        """
        super().__init__()
        self.amc13 = AMC13()
        self.block_id = 'CDF'

    def setup(self):
        """
        Define self header and trailer fields (one 64-bit word each)
        """
        self.header_fields = {'BOE N'   :'u4',
                              'EVT TYPE':'u4',
                              'L1A ID'  :'u24',
                              'BX ID'   :'u12',
                              'SRC ID'  :'u12',
                              'FOV'     :'u4',
                              'H'       :'u1',
                              'X'       :'u1',
                              'S'       :'u2'}

        self.trailer_fields = {'EOE N'     :'u4',
                               'EVT TYPE'  :'u4',
                               'EVT LGTH'  :'u24',
                               'CRC16'     :'u16',
                               'C'         :'u1',
                               'F'         :'u1',
                               'X'         :'u2',
                               'EVT STATUS':'u4',
                               'TTS'       :'u4',
                               'T'         :'u1',
                               'R'         :'u1',
                               'S'         :'u2'}

    def compile(self):
        """
        Compile self header and trailer fields (one 64-bit word each)
        """
        self.header_compiled = bs.compile(
            ''.join(list(self.header_fields.values())),
            list(self.header_fields.keys()))
        self.trailer_compiled = bs.compile(
            ''.join(list(self.trailer_fields.values())),
            list(self.trailer_fields.keys()))

    def unpack(self, stream):
        """
        Unpack CDF block. Unpacking sequence:

        * CDF header
        * AMC13 block
        * CDF trailer

        :param IndexedNDArray stream: An array of 64-bits data words

        :return dict: Dictionary in form ``{Column name tuple: Value}``
        """
        event = {self.update_key(k, 'HEADER'): v
                 for k, v in
                 self.unpack_word(stream, self.header_compiled).items()}
        event.update(self.amc13.unpack(stream))
        event.update({self.update_key(k, 'TRAILER'): v
                      for k, v in
                      self.unpack_word(stream, self.trailer_compiled).items()})
        self.logger.debug("event reading done. Stream position %d", stream.pos)
        return event
